CREATE TABLE archivos(
    cod_archivos serial PRIMARY KEY,
    nombre_archivo varchar(255),
    ruta varchar(255) UNIQUE,
    nombre varchar(255),
    correo varchar(255),
    localizacion_traducida varchar(255),
    localizacion_original varchar(255)
);
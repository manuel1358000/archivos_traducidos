var expect = require("chai").expect;
var request = require("request");
//let server = require('../index.js');
before(done => {
    console.log('\n\n-----------------------\n--\n-- START TEST\n--\n-------------------------');
    done();
});
after(done => {
    console.log('\n\n-----------------------\n--\n-- END TEST\n--\n-------------------------');
    done();
});
describe("Verificar que las rutas funcionen", function () {
    describe("Ruta prueba", function () {
        var url = "http://localhost:5003/";
        it("Retorne 200", function (done) {
            request.get(url, function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });
});
const gulp=require('gulp');
const zip=require('gulp-zip');
const fileindex = require('gulp-fileindex');

gulp.task('empaquetar',function (){
    return gulp.src('src/*')
    .pipe(zip('archivos_traducidos.zip'))
    .pipe(gulp.dest('dist'));
    
});
gulp.task('generar',function (){
    return gulp.src('dist/*.zip')
    .pipe(fileindex())
    .pipe(gulp.dest('./dist'));
});
gulp.task('default',gulp.series('empaquetar','generar'));

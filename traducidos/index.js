const express = require("express");
const bodyParser = require('body-parser');
const app = express();
var request=require('request');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var fs=require('fs');
var path = require('path');
var conexionbd=require('./src/conexiondb.js');
var token=require('./src/verificarToken.js');
var sys = require('util')
var exec = require('child_process').exec;
app.use(express.static('src')); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
conexionbd.client.connect().catch(e=>{
	console.log('Error al conectar con la base de datos '+e.toString());
});

function solicitarToken(){

}

function generarToken(){
    //promesa que nos sirve para consumir el servicio de ubicacion del piloto
    const promise=new Promise(function(resolve,reject){    
        //se consume el servicio por medio de un request POST, en el cual le enviamos en el body el json con la informacion del cliente
        request({
            method:'POST',
            url:'http://'+process.env.NODE_JWT_IP+':'+process.env.NODE_JWT_PORT+'/post/autorizacion',
            body:'{"clientid":"ARCHIVOS_TRADUCIDOS","password":"ARCHIVOS_TRADUCIDOS"}',
            headers:{"Content-Type": "application/json"}
        },function(error,response,body){
            //si ocurre un error al solicitar la respuesta, se muestra la alerta
            if(error!=null){
            	console.log(error);
                reject("{'estado':'404','mensaje': 'Ruta no disponible'}");
            }else{
                resolve(body);    
            }
        });
    });
    return promise;
}
generarToken().then(function(token){
    suscripcion(JSON.parse(token).data.token);
    //suscripcion(token);
});
function suscripcion(token){
    var json='{"token":"'+token+'","url":"http://'+process.env.NODE_ALMACENAMIENTO_IP+':'+process.env.NODE_ALMACENAMIENTO_PORT+'/post/suscripcion","tipo":"POST","funcionSolicitada":"almacenamiento.suscripcion","parametros":{"token":"'+token+'","ip":"'+process.env.NODE_ARCHIVOS_IP+':'+process.env.NODE_TRADUCIDOS_PORT+'"}}';
    const promise=new Promise(function(resolve,reject){    
        //se consume el servicio por medio de un request POST, en el cual le enviamos en el body el json con la informacion del cliente
        request({
            method:'POST',
            url:'http://'+process.env.NODE_ESB_IP+':'+process.env.NODE_ESB_PORT+'/post/comunicacion',
            body:json,
            headers:{"Content-Type": "application/json"}
        },function(error,response,body){
            //si ocurre un error al solicitar la respuesta, se muestra la alerta
            if(error!=null){
            	console.log(error);
                reject({'estado':'404','mensaje': 'Ruta no disponible'});
            }else{
                console.log('Respuesta');
                console.log(body);
                resolve('Suscripcion '+body);    
            }
        });
    });
    return promise;
}

app.get('/',function(req,res){
    console.log(__dirname);
    res.sendFile(path.join(__dirname+'/src/template/index.html'));
});
app.get('/descargar/:nombre',function(req,res){
    //console.log(getDirectories(__dirname+'/complementos'));
    res.download('/app/complementos/'+req.params.nombre,req.params.nombre, function(err){
        console.log(err);
     });
});

function getDirectories(srcpath) {
    return fs.readdirSync(srcpath).filter(function(file) {
      return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
}
app.post('/post/complementoTraducido',function(req,res){
    token.verificarToken(req.body.token).then(function(respuesta){
        if(respuesta){
            crearComplemento(req.body.complemento.nombre.replace(/ /g, ""),req.body.complemento.contenido,req.body.complemento.localizacionTraducida).then(function(nombre_archivo){
                if(nombre_archivo!=''){
                    agregarComplemento(req.body.complemento.nombre.replace(/ /g, ""),nombre_archivo,req.body.nombre,req.body.correo,req.body.complemento.localizacionTraducida,req.body.complemento.localizacionOriginal).then(function(creacion){
                        if(creacion){
                            res.send("{'codigo':'200','mensaje':'OK'}");
                        }else{
                            res.send("{'codigo':'501','mensaje':'Error al crear el archivo complemento'}");        
                        }
                    });    
                }else{
                    res.send("{'codigo':'501','mensaje':'Error al crear el archivo complemento'}");
                }
            });
        }else{
            res.send("{'estado':'401','mensaje': 'Token expirado o sin permisos'}");
        }
    });
});

async function crearComplemento(nombre, contenido,localizacion){
    //metodo que permite crear el complemento en un archivo fisico
    var nombre_archivo=__dirname+'/complementos/'+nombre+'_'+localizacion;
    var contenido_archivo='';
    for(var i=0;i<contenido.length;i++){
        contenido_archivo+='msgid "'+contenido[i].msgid+'"\n';
        contenido_archivo+='msgstr "'+contenido[i].msgstr+'"\n\n';
    }
    fs.writeFile(nombre_archivo+'.po',contenido_archivo, function (err) {
        if (err){
            console.log(err);
            nombre_archivo='';
        };
        console.log('Archivo Creado correctamente');
      });
      //puts('/app/complementos/');
      dir = exec("msgfmt "+nombre_archivo+".po -o "+nombre_archivo+".mo", function(err, stdout, stderr) {
        if (err) {
          //should have err.code here?
          console.log('Error verificar aqui');  
          console.log('Error en msgfmt '+err);
        }
      });
      if(nombre_archivo==''){
          return nombre_archivo;
      }else{
        return nombre_archivo+'.mo';
      }
    
}
async function agregarComplemento(nombre_archivo,ruta,nombre,correo,traducida,original){
    try{
		const resultados=await conexionbd.client.query("insert into archivos(NOMBRE_ARCHIVO,RUTA,NOMBRE,CORREO,LOCALIZACION_TRADUCIDA,LOCALIZACION_ORIGINAL)values('"+nombre_archivo+"','"+ruta+"','"+nombre+"','"+correo+"','"+traducida+"','"+original+"')");
        console.log(resultados);
        return true;
	}catch(w){
        if(w.code==23505){
            return true;
        }else{
            return false;
        }
	}
}
io.on('connection', function(socket) {
    socket.on('pedir_complementos',async function(data){
        try {
            const resultados=await conexionbd.client.query("select * from archivos;");
            socket.emit('complementos',resultados.rows);
        } catch (err) {
            console.error(err);
        }
    });
});


server.listen(process.env.NODE_TRADUCIDOS_PORT,process.env.NODE_TRADUCIDOS_IP, () => {
    console.log("Servidor Archivos Traducidos en la direccion "+process.env.NODE_TRADUCIDOS_IP+":"+process.env.NODE_TRADUCIDOS_PORT);
});
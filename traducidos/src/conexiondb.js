var {Client}=require('pg');
var config={
    host     : process.env.NODE_BD_IP,
	user     : 'postgres',
	password : 'postgres',
    database : 'sa',
    port: process.env.NODE_BD_PORT
};
const client=new Client(config);
module.exports.client=client;
function cargarComplementos(){
    var tabla=document.getElementById('tabla_complementos');
    socket.emit('pedir_complementos',null);
    socket.on('complementos', function(data) {
    var info_tabla='';
    for(var i=0;i<data.length;i++){
		info_tabla+='<tr><th>'+data[i].nombre_archivo+'</th>';
		info_tabla+='<th>'+data[i].nombre+'</th>';
        info_tabla+='<th>'+data[i].correo+'</th>';
        info_tabla+='<th>'+data[i].localizacion_original+'</th>';
        info_tabla+='<th>'+data[i].localizacion_traducida+'</th>';
        info_tabla+='<td><a type="button" href="/descargar/'+data[i].nombre_archivo+'_'+data[i].localizacion_traducida+'.mo" class="btn btn-primary">Descargar .mo</a></td>';
        info_tabla+='<td><a type="button" href="/descargar/'+data[i].nombre_archivo+'_'+data[i].localizacion_traducida+'.po" class="btn btn btn-success">Descargar .po</a></td></tr>';
        console.log(data[i].ruta);
    }	
	tabla.innerHTML=info_tabla;
    }); 
}
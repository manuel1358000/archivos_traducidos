var jwt=require('jsonwebtoken');
var fs=require('fs');
var publicKEY  = fs.readFileSync('./public.key', 'utf8');
var verifyOptions={
    expiresIn:'1h',
    algorithm:['RS356']
};
function verificarToken(token){
    const promise=new Promise(function(resolve,reject){    
        //se tiene que realizar la verificacion del acceso, fecha de expiracion y scope
        var legit=jwt.verify(token,publicKEY,function(err,decoded){
            if(err){
                resolve('');
            }else{
                console.log(decoded);
                resolve(true);
            }
        });    
    });
    return promise;
}
module.exports.verificarToken=verificarToken;